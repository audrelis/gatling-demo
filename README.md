# Gatling Performance Tests for Spring PetClinic Sample Application

Gatling project to test performance of the spring petclinic application.

## Prerequisites
1. Java Development Kit and Maven installed
2. spring-petclinic project, which is located [here](https://github.com/spring-projects/spring-petclinic)
3. IDE with Scala support for editing the source code.

## Setup
1. Clone spring petclinic project with the following command :
```
git clone https://github.com/spring-projects/spring-petclinic.git
```
2. Clone this project with the following command:
```
https://gitlab.com/audrelis/gatling-demo
```

## Running the tests
1. Start spring petclinic application on localhost by following instructions in [project site](https://github.com/spring-projects/spring-petclinic)
2. Navigate to directory where Gatling project has been cloned to and execute command:
```
mvn test
```