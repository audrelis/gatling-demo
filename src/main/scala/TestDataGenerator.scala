import com.typesafe.scalalogging.slf4j.Logger
import fabricator.Fabricator
import fabricator.enums.CsvValueCode
import org.slf4j.LoggerFactory

object TestDataGenerator {

  val logger = Logger(LoggerFactory.getLogger("TestDataGenerator"))
  val NUMBER_OF_ROWS_TO_GENERATE = 10000

  val csvFilePath: String = "target/generated-files/new_owners.csv"
  lazy val file = Fabricator.file()

  def main(args: Array[String]): Unit = {
    logger.info("Started generating pet owners CSV file")

    val values: Array[Any] = Array(CsvValueCode.FIRST_NAME, CsvValueCode.LAST_NAME, CsvValueCode.ADDRESS, "New York", "123456789", "Spike", "2016/01/01", "dog")

    file.csvBuilder.withNumberOfRows(NUMBER_OF_ROWS_TO_GENERATE)
      .withCustomCodes(values)
      .withTitles(Array("first_name", "last_name", "address", "city", "telephone", "pet_name", "birth_date", "pet_type"))
      .saveTo(csvFilePath)
      .build()

    logger.info("Finished generating pet owners CSV file")
  }

}