package petclinic.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.check.HttpCheck
import petclinic.config.Config
import petclinic.util.RandomUtil

object OwnerRequest {

  def selectRandomOwner(): HttpCheck = {
    css("table[id='owners'] a", "href").findAll.transform(RandomUtil.random).optional.saveAs("ownerLink")
  }

  def selectRandomVisitLink(): HttpCheck = {
    css("a[href$='/visits/new']", "href").findAll.transform(RandomUtil.random).optional.saveAs("visitLink")
  }

  val showLanding = {
    exec(http("Owners page")
      .get("/petclinic/owners/find.html"))
  }

  val navigateToNewOwnerForm = {
    exec(http("Navigate to New owner form")
      .get("/petclinic/owners/new")
    )
  }

  def addNewOwner(firstName: String, lastName: String, address: String, city: String, telephone: String) = {
    exec(http("Add a new Owner")
      .post("/petclinic/owners/new")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .formParam("firstName", firstName)
      .formParam("lastName", lastName)
      .formParam("address", address)
      .formParam("city", city)
      .formParam("telephone", telephone)
      .check(substring(lastName))
      .check(css("a:contains('Add New Pet')", "href").saveAs("addPetLinkSuffix")))
  }

  val listOwners = {
    exec(http("List all owners")
      .get("/petclinic/owners.html?lastName=")
    )
  }

  val exportOwners = {
    exec(http("Export owners to PDF")
      .get("/petclinic/owners.html?lastName=&dtt=f&dti=owners&dtf=pdf&dtp=y&dandelionAssetFilterState=false")
    )
  }

  def findOwner(lastName: String) = {
      exec(http("Find owner by last name")
        .get("/petclinic/owners.html")
        .queryParam("lastName", lastName)
        .check(substring("has not been found").optional.saveAs("ownerNotFound"))
        .check(substring("Owner Information").optional.saveAs("oneOwnerFound"))
        .check(substring("Search:").optional.saveAs("moreThanOneOwnerFound"))
        .check(
          checkIf((response: Response, session: Session) => session.contains("moreThanOneOwnerFound"))(selectRandomOwner())
        )
        .check(
          checkIf((response: Response, session: Session) => session.contains("oneOwnerFound"))(selectRandomVisitLink())
        )
      )
      .doIf("${moreThanOneOwnerFound.exists()}") {
          pause(Config.DEFAULT_THINK_TIME_SECS)
          exec(http("Get a random owner")
            .get("${ownerLink}")
            .check(selectRandomVisitLink()))
      }
  }

}