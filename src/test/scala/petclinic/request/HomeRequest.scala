package petclinic.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object HomeRequest {

  val showLanding = {
    exec(http("Home page")
      .get("/petclinic"))
  }

}