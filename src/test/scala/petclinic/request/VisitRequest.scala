package petclinic.request

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import petclinic.util.RandomUtil

object VisitRequest {

  def extractPetIdFromUrl(): String = {
    val pattern = """.*/pets/(.*)/.*/.*""".r
    val pattern(petId) = "${visitLink}"
    petId
  }

  val navigateToNewVisitForm = {
    exec(http("Navigate to New visit form")
      .get("${visitLink}"))
  }

  val addNewVisit = {
    val formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd")

    exec(http("Add a new visit")
      .post("${visitLink}")
      .formParam("date", session => RandomUtil.randomDate(LocalDate.now(), LocalDate.now().plusMonths(3)).format(formatter))
      .formParam("description", session => RandomUtil.randomString(20))
      .formParam("petId", extractPetIdFromUrl))
  }

}