package petclinic.request

import io.gatling.core.Predef._
import io.gatling.core.session
import io.gatling.http.Predef._


object PetRequest {

  val navigateToNewPetForm = {
    exec(http("Navigate to New pet form")
        .get("/petclinic/owners/${addPetLinkSuffix}"))
  }

  def addNewPet(pet_name: String, birth_date: String, pet_type: String) = {
    exec(http("Add a new Pet")
      .post("/petclinic/owners/${addPetLinkSuffix}")
      .header("Content-Type", "application/x-www-form-urlencoded")
      .formParam("name", pet_name)
      .formParam("birthDate", birth_date)
      .formParam("type", pet_type)
      .check(substring(pet_name))
    )
  }

}
