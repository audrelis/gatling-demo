package petclinic.request

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object VetRequest {

  val listVets = {
    exec(http("Vets list page")
      .get("/petclinic/vets.html")
      .check(substring("Sharon")))
  }

  val getVetsXml = {
    exec(http("Vets XML")
      .get("/petclinic/vets.xml")
      .check(bodyString.is(ElFileBody("vets.xml"))))
  }

  val getVetsJson = {
    exec(http("Vets JSON")
      .get("/petclinic/vets.json")
      .check(bodyString.is(ElFileBody("vets.json"))))
  }

}