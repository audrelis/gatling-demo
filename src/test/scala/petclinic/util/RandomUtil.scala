package petclinic.util

import java.time.LocalDate
import java.time.temporal.ChronoUnit._

import scala.util.Random

object RandomUtil {

  def randomDate(from: LocalDate, to: LocalDate): LocalDate = {
    val diff = DAYS.between(from, to)
    from.plusDays(Random.nextInt(diff.toInt))
  }

  def randomString(length: Int): String = {
    Random.alphanumeric.take(length).mkString
  }

  val random = (list: Seq[String]) => {
    list(Random.nextInt(list.size))
  }

}