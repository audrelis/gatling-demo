package petclinic

import io.gatling.core.Predef._
import petclinic.config.Config
import petclinic.request.{OwnerRequest, PetRequest, VisitRequest}

import scala.concurrent.duration._

class PetClinicSimulation extends Simulation {

  val httpProtocolConfig = Config.HTTP_PROTOCOL

  val ownersFeeder = csv("owners.csv").random

  val newVisitScenario = scenario("Register new visit scenario").feed(ownersFeeder)
    .exec(OwnerRequest.showLanding)
    .pause(Config.DEFAULT_THINK_TIME_SECS)
    .repeat(Config.REPEAT_COUNT) {
        exec(OwnerRequest.findOwner("${last_name}")
        .pause(Config.DEFAULT_THINK_TIME_SECS))
        .doIf("${visitLink.exists()}") {
          exec(VisitRequest.navigateToNewVisitForm)
          .pause(Config.DEFAULT_THINK_TIME_SECS)
          .exec(VisitRequest.addNewVisit)
        }
    }

  val addOwnerScenario = scenario("Add new owner scenario").feed(ownersFeeder)
    .exec(OwnerRequest.showLanding)
    .pause(Config.DEFAULT_THINK_TIME_SECS)
    .exec(OwnerRequest.addNewOwner("${first_name}", "${last_name}", "${address}", "${city}", "${telephone}"))
    .pause(Config.DEFAULT_THINK_TIME_SECS)
    .exec(PetRequest.navigateToNewPetForm)
    .pause(Config.DEFAULT_THINK_TIME_SECS)
    .exec(PetRequest.addNewPet("${pet_name}", "${birth_date}", "${pet_type}"))

  setUp(
    newVisitScenario.inject(constantUsersPerSec(Config.USERS_PER_SEC) during (Config.DURATION seconds)),
    addOwnerScenario.inject(constantUsersPerSec(Config.USERS_PER_SEC) during (Config.DURATION seconds))
  )
  .protocols(httpProtocolConfig)
  .assertions(global.responseTime.max.lessThan(2000), global.successfulRequests.percent.is(100))

}