package petclinic.config

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Config {

  val SERVER_HOST = "localhost"

  val SERVER_PORT = 9966

  val DEFAULT_THINK_TIME_SECS = 5

  val USERS_PER_SEC = 2

  val DURATION = 30

  val REPEAT_COUNT = 2

  val HTTP_PROTOCOL = http
      .baseURL(serverUrl())
      .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
      .acceptEncodingHeader("gzip, deflate, sdch")
      .acceptLanguageHeader("en-US,en;q=0.8,cs;q=0.6,lt;q=0.4")
      .connectionHeader("keep-alive")
      .header("Upgrade-Insecure-Requests", "1")
      .userAgentHeader("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
      .inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""",
        """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png"""), WhiteList())

  def serverUrl(): String = {
    "http://" + SERVER_HOST + ":" + SERVER_PORT
  }

}
